FROM gradle:jdk11-alpine as build
COPY . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build

FROM openjdk:8-jdk-alpine3.9
RUN mkdir /app
COPY --from=build /home/gradle/src/build/libs/*.jar /app/querist.jar
ENTRYPOINT ["java", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-Djava.security.egd=file:/dev/./urandom","-jar","/app/querist.jar"]
