package org.k3;

import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

public class App
{
    public static void main( String[] args ) {
        if (args.length != 2) {
            System.out.println("please input botname and token");
            System.exit(1);
        }
        try {
            TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
            botsApi.registerBot(new Querist(args[0], args[1]));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
