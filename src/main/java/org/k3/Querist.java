package org.k3;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import java.util.ArrayList;

public class Querist extends TelegramLongPollingBot {
    private String botname = "";
    private String bottoken = "";
    public Querist(String botname, String bottoken) {
        this.botname = botname;
        this.bottoken = bottoken;
    }

    public void sendMessage(Long who, String what) {
        SendMessage message = SendMessage.builder()
                .parseMode("MarkdownV2")
                .chatId(who.toString())
                .text("`" + what + "`")
                .build();
        try {
            execute(message);
        } catch (TelegramApiException e) {
            throw new RuntimeException(e);
        }
    }
    @Override
    public void onUpdateReceived(Update update) {
        if(update.hasMessage() && update.getMessage().hasText()){
            sendMessage(update.getMessage().getChatId(), "hello");
        }
    }

    @Override
    public String getBotUsername() {
        return botname;
    }
    @Override
    public String getBotToken(){
        return bottoken;
    }
    @Override
    public void onRegister() {
        super.onRegister();
    }
}
